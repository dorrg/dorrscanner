#Scanner

#Summary#
This program users JFlex as the back bone in order to create a Scanner. JFlex takes in a particular language and grammar defended by the user and converts it into java source code. The Scanner is designed to scan Mini-pascal. It'll take in a text file and output tokens to be used later in the compiler.