/**
 *	This is a simple example of a jflex lexer definition
 */

/* Declarations */

%%


%class MyScanner 	/* names produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
%{
	LookUpTable map = new LookUpTable();
%}


/* Patterns */
other			  = .
letter		 	  = [A-Za-z]
digit 		 	  = [0-9]
whitespace   	  = [ \n\t\r]|(([\{])([^\{])*([\}]))
symbol 		 	  = [;,.:\[\]()+-=<>\*/] 
id 		 	 	  = ({letter}+)({letter}|{digit})*
symbols 	 	  = {symbol}|:=|<=|>=|<>
digits 		 	  = ({digit})({digit}*)
optional_fraction = ([.])({digits})
optional_exponent = ([E]([+]|[-])?{digits})
num 			  = {digits}{optional_fraction}?{optional_exponent}?{optional_fraction}?

%%
/* Lexical Rules */
{id}		{
				Keywords key = map.get(yytext());
				if(key == null){
					return (new Token(yytext(), Keywords.ID));
				}
				else{
					return (new Token(yytext(), key));
				}
				
			}
			
{whitespace}	{ 
					/*ignore whitespace*/
					if((yytext().charAt(0)=='{')&&(yytext().charAt(yytext().length()-1)=='}'))
						System.out.println("Comment: " + yytext());
				}

{num}		{
				return new Token(yytext(),Keywords.NUMBER);
			}

{symbols}	{
				Keywords key = map.get(yytext());
				return(new Token(yytext(),key));
			}

{other}		{
				System.out.println("Illegal char: '" + yytext() + "' found.");
			}